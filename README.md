<p><img width="706px" src="546face.jpg" title="人工智能与开源"></p>

# AI & OpenSource

#### 介绍
《人工智能与创客》分上下两册，是谢开年博士撰写的青少年创客教育的优秀教材。SIGer.sh 作为创客教育的开源实践，同样得到了谢博的大力支持，本项目作为学习过该书的同学，分享学习笔记的开源仓，采用共同写作的方式，以《人工智能与开源》为题汇聚更多的优秀案例，以期能够在未来谢博的最新著作中得以收录！

#### 人工智能与创客（上）

奇妙的3D打印乐园
- 走进3D打印的世界
- 学习3D建模软件

魔幻的电子积木
- 电子积木的搭建与编译
- 小组创造OTTO机器人

#### 人工智能与创客（下）

app应用的探索与开发
- app inventor
- app inventor与人工智能

你好！python的世界
- python编程
- 开源硬件
- 人工智能
- 创意思维

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
